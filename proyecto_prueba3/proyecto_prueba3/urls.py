"""proyecto_prueba3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app_prueba3 import views

urlpatterns = [
    path('admin/', admin.site.urls, name="admin"),
    path('ingresar_paquete/', views.ingresar_paquete, name="ingresar_paquete"),
    path('listar_paquetes/', views.listar_paquetes, name="listar_paquetes"),
    path('borrar_paquete/', views.borrar_paquete, name="borrar_paquete"),
    path('modificar_paquete/', views.modificar_paquete, name="modificar_paquete"),
    path('ingresar/', views.ingresar, name="ingresar"),
    path('listar/', views.listar, name="listar"),
    path('borrar/', views.borrar, name="borrar"),
    path('borrar2/<numero_despacho2>', views.borrar2, name="borrar2"),
    path('modificar/', views.modificar, name="modificar"),
    path('index/', views.index_html, name="index"),
]
