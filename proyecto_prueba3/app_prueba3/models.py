from django.db import models
from django.db.models.fields import NullBooleanField

# Create your models here.
class Paquete(models.Model):
    numero_despacho = models.CharField(max_length=20, primary_key=True, db_index=True)
    nombre_cliente = models.CharField(max_length=30)
    direccion = models.CharField(max_length=50)
    telefono = models.CharField(max_length=12)
    producto = models.CharField(max_length=10000)
    peso = models.FloatField()
    alto_paquete = models.FloatField()
    ancho_paquete= models.FloatField()
    profundida_paquete = models.FloatField()
    fecha_ingreso = models.DateField()
    fecha_envio = models.DateField(blank=True, null=True)
    estado = models.BooleanField()
    
    def __Str__(self):
        return self.numero_despacho